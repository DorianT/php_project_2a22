
<?php
  include("header.php");
  include("ConnectBDD.php");

  $id = $_GET['id'];
  $result = $bdd->query("Select * from films where code_film =".$id );
  foreach($result as $i) {
    echo "<h1>".$i['titre_francais']."</h1>";
    echo "<br> Titre original : ".$i['titre_original']."</br>";
    echo "<br> Titre original : ".$i['pays']."</br>";
    echo "<br> Durée : ".$i['duree']."</br>";
    echo "<br> Année de sortie : ".$i['date']."</br>";
    echo "<br> Couleur ou NB : ".$i['couleur']."</br>";
    $author_array = $bdd->query("Select nom,prenom from individus where code_indiv =".$i['realisateur']);
    foreach ($author_array as $a) {
      echo "<br> Réalisateur : ".$a['nom']." ".$a['prenom']."</br>";
    }
  }
  //Genres
  echo "<br> Genres : </br>";
  $result = $bdd->query("Select distinct ref_code_genre as rcf from classification where ref_code_film=".$id);
  foreach($result as $i) {
    $idG = $i['rcf'];
    $genres = $bdd->query("Select nom_genre as n from genres where code_genre=".$idG);
    foreach ($genres as $g) {
      echo "<br>";
      echo " - ".$g['n'];
      echo "</br>";
    }
  }
  //acteurs
  echo "<br> Acteurs : </br>";
  $result = $bdd->query("Select distinct ref_code_acteur as rca from acteurs where ref_code_film=".$id);
  foreach($result as $i) {
    $idA = $i['rca'];
    $acteurs = $bdd->query("Select nom,prenom from individus where code_indiv=".$idA);
    foreach ($acteurs as $a) {
      echo "<br>";
      echo " - ".$a['nom']." ".$a['prenom'];
      echo "</br>";
    }
  }
  echo "<form method=\"POST\" action=\"modif.php?id=".$id."\">";
?>
  <button type="submit" name="submit" value="Modifier">Modifier</button>
  </form>
